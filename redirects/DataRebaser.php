<?php
class DataRebaser{
    public $path_seo_files = 'seo_files/';
    public $path_generated_files = 'files/';
    
    
    public function getSeoFileNames(){
        $file_names = scandir($this->path_seo_files);
        
        foreach ($file_names as $index => $file_name) {
            if ( $file_name[0] == '.' ) {
                unset($file_names[$index]);
            }
        }
        
        return $file_names;
    }
    
    public function getFiles(){
        $file_names = scandir($this->path_generated_files);
        
        foreach ($file_names as $index => $file_name) {
            if ( $file_name[0] == '.' ) {
                unset($file_names[$index]);
            }
        }
        
        return $file_names;
    }
    
    
    // alias | to do | redirect-to
    // [0]                      [1]                 [2]
    // /articles             | удалить | 
    // /articles/old_article | 301 |        /articles/new_article
    
    public function csv_to_array($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        
        foreach ($file as $key=>$str) {
            $tmp = str_getcsv($str,",");
            array_push($keys, $tmp);
        }
        $keys = '<?php return ' . var_export($keys, true) . '?>';
        file_put_contents(
            $this->path_generated_files . explode('.', $filename)[0] . '.txt', $keys
        );
    }
    
}
