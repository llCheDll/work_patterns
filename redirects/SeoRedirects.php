<?php

class SeoRedirects {
    public $curr_uri;
    public $curr_host;
    public $final_host;
    public $final_uri;
    public $rebaser;
    public $redirect_files_path;


    public function __construct($uri, $host) {
        $this->curr_host = $host;
        $this->curr_uri = $uri;
        $this->final_host = $this->curr_host;
        $this->final_uri = $this->curr_uri;
        
        require_once('think-client-service/data/redirects/DataRebaser.php');
        $this->rebaser = new DataRebaser();
        
        if (!$this->rebaser->getFiles()) {
            $file_names = $this->rebaser->getSeoFileNames();
            
            foreach ($file_names as $names) {
                $this->rebaser->csv_to_array($names);
            }
        }
        
        $this->redirect_files_path = $this->rebaser->path_generated_files;
    }

    public function wmRedirect301($query_string) {
        if (!empty($query_string)) {
            $query_string = '?' . $query_string;
        }
        
        if (strpos($this->final_uri, '://')){
            header(
                'Location: ' . $this->final_uri , true, 301
            );
            die();
        } else {
            header(
                'Location: http://' . $this->final_host . $this->final_uri . $query_string, true, 301
            );
            die();
        }
    }

    public function wmCheckRedirect($query_string, $_https) {
        if (preg_match('#gclid#i', $query_string) != 1 &&
            preg_match('#utm_#i', $query_string) != 1){
            
            preg_match('/^(.*?\.html.*?)\.html$/s', $this->final_uri, $find_slashes);
        
            if(isset($find_slashes[1])) {
                $tmp_find_sla = str_replace('.html', '', $find_slashes[1]);
                $this->final_uri = $tmp_find_sla . '.html';
            }
            
            if ($this->curr_uri != $this->final_uri || $this->curr_host != $this->final_host) {
                $this->wmRedirect301($query_string);
            } else if($_https) {
                $this->wmRedirect301($query_string);
            }
        }
    }

    public function getAlises($filename){
        return include($this->redirect_files_path . $filename);
    }
    
    public function wmCheckFileRedirect(){
        $aliases = array();
        
        if (strpos($this->curr_uri, 'ua/') !== false){
            $aliases = $this->getAlises('ua.txt');
        } else {
            $aliases = $this->getAlises('ru.txt');
        }
        
        $tmp_url = 'https://' . $this->curr_host . $this->final_uri;
        
        foreach ($aliases as $rule) {
            if ($rule[0] === $tmp_url) {
                if ($rule[2]) {
                    $this->final_uri = $rule[2];
                    $this->wmCheckRedirect('', false);
                }
            }
        }
    }
    

    public function validateRedirects() {
        // $notRedirectForUrls = array(
        // // 'WayForPay'
        // );

        // $notRedirectForUrlsStatus = true;

        // foreach ($notRedirectForUrls as $val) {
        //     if(strpos($_SERVER['REQUEST_URI'], $val) !== false) {
        //         $notRedirectForUrlsStatus = false;
        //     }
        // }
        // if(strpos($this->curr_host, 'www.') !== false) {
        //     $this->final_host = str_replace('www.', '', $this->curr_host);
        // }
        
        $this->final_uri = $this->curr_uri;
        $query_string = '';
        
        if (strpos($this->curr_uri, '?') !== false){
            $parts = explode('?', $this->curr_uri);
            $query_string = end($parts);
            $this->curr_uri = reset($parts);
            $this->final_uri = $this->curr_uri;
        }
        
        //--- C много слешей на один ---//
        if (strpos($this->final_uri, '//') !== false) {
            while (strpos($this->final_uri, '//') !== false) {
                $this->final_uri = str_replace('//', '/', $this->final_uri);
            }
        }
        
        //--- C верхнего регистра на нижний ---//
        if (preg_match('#[A-Z]#su', $this->final_uri)) {
            $this->final_uri = strtolower($this->final_uri);
        }
        
        //--- C index.php на без index.php ---//
        if (strpos($this->final_uri, "/index.php") !== false && empty($_GET)){
            $this->final_uri = str_replace('/index.php', '', $this->final_uri);
        }
        
        // if (strpos($this->final_uri, "/index.php") !== false &&
        //     !empty($_GET['route']) &&
        //     $_GET['route'] == 'common/home'){
        //     var_dump('expression');
        //     $query_string = '';
        //     $this->final_uri = str_replace('/index.php', '', $this->final_uri);
        // }

        // //--- C без .htm в конце на .html в конце ---//
        // if (substr($this->final_uri, -4) == ".htm") {
        //     $this->final_uri = $this->final_uri . 'l';
        // }
        
        //--- C / в конце на без / в конце ---//
        // if(substr($this->final_uri, -1) == '/' && $this->final_uri !== '/'){
        //     $this->final_uri = substr_replace($this->final_uri ,"",-1);
        // }
        
        // if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' ||
        //     $_SERVER['HTTPS'] == 1) || 
        //     isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && 
        //     $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
        //     $this->wmCheckRedirect($this->final_uri, $query_string, true);
        // } else {
        $this->wmCheckRedirect($query_string, false); 
        // }
    }
}
