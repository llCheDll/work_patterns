<?php
class DataRebaser{
    public $path_seo_files = 'seo_files/';
    public $path_genereted_files = 'files/';
    
    public $html;
    public $keys;
    
    public function __construct() {
        $this->html = '<meta charset="utf-8"><div class="wm_site_map">';
        $this->keys = array();
    }
    
    public function getSeoFileNames(){
        $file_names = scandir($this->path_seo_files);
        
        foreach ($file_names as $index => $file_name) {
            if ( $file_name[0] == '.' ) {
                unset($file_names[$index]);
            }
        }
        
        return $file_names;
    }
    
    public function getKeys($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $ul_flag = 0;
        
        foreach ($file as $key=>$str) {
            if (strpos($filename, 'ru') === false){
                $tmp = str_getcsv($str,",");
                array_push($this->keys, $tmp);
            }
        }
    }
    
    public function addLi($text, $href) {
        $html_pattern = '<li><a href="%s">%s</a></li>';
        $html = sprintf($html_pattern, $href, $text);
        $this->html .= $html;
    }
    
    public function getHtml($first_countre=0, $second_counter=1){
        $ul_flag = 0;
        foreach ($this->keys as $key => $value) {
            if ($value[0]) {
                $ul_flag += 1;
                $this->html .= '<ul>';
                $this->addLi($value[0], $value[1]);
            } elseif ($value[1]) {
                if ($ul_flag == 1) {
                    $this->html .= '<ul>';
                    $ul_flag += 1;
                } elseif ($ul_flag == 3) {
                    $this->html .= '</ul>';
                    $ul_flag -= 1;
                }
                $this->addLi($value[1], $value[2]);
            } elseif ($value[2]) {
                if ($ul_flag == 2) {
                    $this->html .= '<ul>';
                    $ul_flag += 1;
                } elseif($ul_flag == 4) {
                    $this->html .= '</ul>';
                    $ul_flag -= 1;
                }
                $this->addLi($value[2], $value[3]);
            } elseif ($value[3]) {
                if ($ul_flag == 3) {
                    $this->html .= '<ul>';
                    $ul_flag += 1;
                } elseif($ul_flag == 5) {
                    $this->html .= '</ul>';
                    $ul_flag -= 1;
                }
                $this->addLi($value[3], $value[4]);
            } else {
                if ($ul_flag == 1) {
                    $this->html .= '</ul>';
                    $ul_flag -= 1;
                }
            }
        }
        $dom = new DOMDocument();

        $dom->preserveWhiteSpace = false;
        $dom->loadHTML($this->html, LIBXML_HTML_NOIMPLIED);
        $dom->formatOutput = true;


        print $dom->saveXML($dom->documentElement);
        // die;
        return $this->html;
        
    }
}
