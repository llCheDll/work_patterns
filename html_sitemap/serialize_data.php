<?php
include_once('DataRebaser.php');

$rebaser = new DataRebaser();

$file_names = $rebaser->getSeoFileNames();

foreach ($file_names as $key => $file_name) {
    $rebaser->getKeys($file_name);
    $rebaser->getHtml();
}

