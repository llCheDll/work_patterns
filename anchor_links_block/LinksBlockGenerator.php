<?php
class LinksBlockGenerator{
    public $num_links_in_block = 6;
    
    public $path_seo_files;
    public $files_path;
    public $hash_uri;
    public $content;
    
    public $anchore_blocks_file; //array() exist blocks
    public $anchore_blocks;
    
    public $anchore_links;
    public $anchore_links_file;                         //array() links (<li><a href>)
    
    
    function __construct($uri='', $filename='vascular.csv', $path=''){ //filename from seo_files
        $this->path_seo_files = $path . '/' . 'seo_files/';
        $this->files_path = $path . '/' . 'files/';
        $this->files_path = 'files/';
        
        $tmp = explode('.', $filename);
        $tmp = $tmp[0];
        
        $this->anchore_links_file = $tmp . '.txt';
        $this->anchore_blocks_file = 'blocks_' . $tmp . '.txt';
        $this->hash_uri = base64_encode($uri);
        $this->content = array();
        
        if ( !file_exists($this->files_path . $this->anchore_links_file) ){
            $this->rebaseSeoFileToArray($filename);
        }

        $this->anchore_links   = include(
            strval($this->files_path . $this->anchore_links_file)
        );
        $this->anchore_blocks  = include(
            strval($this->files_path . $this->anchore_blocks_file)
        );
    }
    
    public function rebaseSeoFileToArray($filename) {
        $file = file(strval($this->path_seo_files . '/' . $filename));
        $keys = array();
        foreach ($file as $key=>$str) {
            $tmp = str_getcsv($str,",");
            $first = rtrim($tmp[1], '/');
            $second = $tmp[0];
            $url = $first . '/#' . strval($key+1). '-' . strval(rand(1, 100));
            $key = "<li><a href=$url style=''>$second</a></li>";
            // $key = $second . ', ' . $url;
            array_push($keys, $key);
        }
        
        $keys = '<?php return ' . var_export($keys, true) . '?>';
        $tmp = explode('.', $filename);
        $tmp = $tmp[0];
        $file_name = $tmp . '.txt';
        
        file_put_contents( $this->files_path . $this->anchore_links_file, $keys);
    }
    
    public function getBlock(){
        //if uri exist returns array with hrefs
        //else return False
        
        $keys = array_keys($this->anchore_blocks);
        
        if (!empty($keys)) {
            foreach ($keys as $value) {
              if ($this->hash_uri == $value) {
                return $this->anchore_blocks[$this->hash_uri];
              }
            }
        } else {
            return False;
        }
    }
    
    public function setElements(){
        $block_elements = array();
        
        foreach ($this->anchore_links as $index => $li) {
            if ($this->num_links_in_block - 1 < $index) {
                break;
            }
            array_push($block_elements, $li);
            unset($this->anchore_links[$index]);
        }
        
        $this->anchore_blocks[$this->hash_uri] = $block_elements;
        
        $this->anchore_links = array_values($this->anchore_links);
        
        file_put_contents(
            $this->files_path . $this->anchore_links_file,
            '<?php return ' . var_export($this->anchore_links, true) . '?>'
        );
        
        
        file_put_contents(
            $this->files_path . $this->anchore_blocks_file,
            '<?php return ' . var_export($this->anchore_blocks, true) . '?>'
        );
        
        return $block_elements;
    }
    
    public function createHTML($block){
        $style = "<style>.lotive-text-hide{height:0px;overflow:hidden;}.lotive-text-show{height:auto;overflow:visible;}</style>";
        array_push($this->content, $style);
        array_push($this->content,'<div id="anchor-block" class="blockLinks">');
        array_push(
            $this->content,
            '<button href="#anchor-links" onclick="lotiveShowHideText()" class="btn btn-info">Популярные запросы</button>'
        );
        array_push($this->content,'<div id="anchor-links" class="lotive-text-hide" >');
        array_push($this->content,'<ul>');
        
        foreach ($block as $li) {
            array_push($this->content, $li);
        }
        
        array_push($this->content, '</ul>');
        array_push($this->content, '</div>');
        array_push($this->content, '</div>');

        array_push(
            $this->content, '<script> function lotiveShowHideText(){$("#anchor-links").toggleClass("lotive-text-show");}</script>'
        );
        
        $this->content = implode(PHP_EOL, $this->content);
    }
    
    public function generates_anchores(){
        $anchore_block = $this->getBlock();
        
        if (!$anchore_block) {
            $anchore_block = $this->setElements();
        }
        $this->createHTML($anchore_block);
    }
    
    public function getHTML(){
        if (!$this->content){
            return False;
        }
        
        return $this->content;
    }
}


// Usage

// $generator = new LinksBlockGenerator(uri, 'filename.csv', $GLOBSALS['anchore_data']);
// $generator->generates_anchores();
// $generator->getHTML();

// this retened ready to use anchore links block 6 links in block
// change $num_links_in_block if you need another quantity

// to change style of <a> tags go to rebaseSeoFileToArray()
// after changes delete files in 'think-client-service/data/anchor_links_block/files/*'
// scripts re-generate base file with <a> tags
