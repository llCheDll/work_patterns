<?php
class DataRebaser{
    public $path_seo_files = 'seo_files/';
    public $path_genereted_files = 'files/';
    
    
    public function getSeoFileNames(){
        $file_names = scandir($this->path_seo_files);
        
        foreach ($file_names as $index => $file_name) {
            if ( $file_name[0] == '.' ) {
                unset($file_names[$index]);
            }
        }
        
        return $file_names;
    }
    
    public function csv_anchores_to_array($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        foreach ($file as $key=>$str) {
            $tmp = str_getcsv($str,",");
            $url = $tmp[1] . '/#' . strval($key+1). '-' . strval(random_int(1, 100));
            $key = "<li><a href=$url>$tmp[0]</a></li>";
            array_push($keys, $key);
        }
        $keys = '<?php return ' . var_export($keys, true) . '?>';
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.txt', $keys
        );
    }
    
    public function csv_onekey_to_array($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        foreach ($file as $str) {
            $key = str_getcsv($str);
            array_push($keys, $key[0]);
        }
        $keys = serialize($keys);
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.txt', $keys
        );
    }
    
    // Functionn rewrite file
    // 2 key in <a href=''>text</a> array
    // 1 column [text] 2 column [href]
    public function csv_twokeys_to_hyperlink($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        foreach ($file as $str) {
            $tmp = str_getcsv($str,",");
            $key = "<a href=$tmp[1]>$tmp[0]</a>";
            array_push($keys, $key);
        }
        $keys = '<?php return ' . var_export($keys, true) . '?>';
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.txt', $keys
        );
    }
    
    // Functionn rewrite file
    // key = Buy_tag tag_buy value = <a href></a>
    // 2 key in <a href=''>text</a> array
    // 1 column [text] 2 column [href]
    public function csv_threekeys_to_dict($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        foreach ($file as $str) {
            $tmp = str_getcsv($str,",");
            $value = "<a href=$tmp[1]>$tmp[0]</a>";
            if (!array_key_exists($tmp[2], $keys)) {
                $keys[$tmp[2]] = array();
            }
            array_push($keys[$tmp[2]], $value);
        }
        $keys = serialize($keys);
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.txt', $keys
        );
    }
    
    public function csv_base_text($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $base_text = array();
        $flag = false;
        foreach ($file as $str) {
            $tmp = str_getcsv($str, ',');
            $counter = 0;
            if(!$flag) {
                foreach ($tmp as $row) {
                    array_push($base_text, array($row));
                }
                $counter++;
                $flag = true;
            } else {
                foreach ($tmp as $row) {
                    if ($row){
                        array_push($base_text[$counter], $row);
                    }
                    $counter++;
                }
            }
        }
        $keys = serialize($base_text);
        // $keys = '<?php' . var_export($base_text, true) . '>';
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.txt', $keys
        );
    }
    
}
