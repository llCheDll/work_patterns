<?php

class TextGenerator{
    public $base;
    public $keys_list;
    public $curr_h1;
    
    public $path = 'files/';

    function __construct($h1){
        $this->curr_h1 = $h1;
        $this->base = file($this->path . 'base.txt');
        $this->base = unserialize($this->base[0]);
        $this->keys_list = $this->get_keys_list();
    }
    
    public function get_keys_list() {
        $tags         = file($this->path . 'tags.txt');
        $tags         = unserialize($tags[0]);
        $cities       = file($this->path . 'cities.txt');
        $tags['city'] = unserialize($cities[0]);
        $tags['curr_h1'] = $this->curr_h1;
        return $tags;
    }
    
    public function generate_text() {
        $generated_text = '';
        
        foreach ($this->base as $text) {
            if ( count($text) == 1 ){
                if ( array_key_exists($text[0], $this->keys_list) ) {
                    if ( $text[0] == 'cities' ) {
                        $cities_indexes = array_rand($this->keys_list['cities'], 2);
                        $generated_text .= ' ' . $this->keys_list['cities'][$cities_indexes[0]] . ', ' . $this->keys_list['cities'][$cities_indexes[1]] . ' ';
                    } elseif ( $text[0] == 'curr_h1' ) {
                        $generated_text .= ' ' . $this->keys_list['curr_h1']. ' ';
                        var_dump($generated_text);die;
                    } else {
                        $generated_text .= ' ' . $this->keys_list[$text[0]][array_rand($this->keys_list[$text[0]])] . ' ';
                    }
                } else {
                    $generated_text .= ' ' . $text[array_rand($text)] . ' ';
                }
            } else {
                $generated_text .= ' ' . $text[array_rand($text)] . ' ';
            }
        }
        
        return $generated_text;
    }
}

?>
