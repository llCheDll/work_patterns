<?php
include_once('DataRebaser.php');
include_once('TextGenerator.php');

$rebaser = new DataRebaser();
$generator = new TextGenerator('TEST_H1');

$file_names = $rebaser->getSeoFileNames();

foreach ($file_names as $key => $file_name) {
    if (strpos($file_name, 'cities') !== false) {
        $rebaser->csv_onekey_to_array($file_name);
    } elseif (strpos($file_name, 'base') !== false) {
        $rebaser->csv_base_text($file_name);
    } elseif (strpos($file_name, 'tags') !== false){
        $rebaser->csv_threekeys_to_dict($file_name);
    }
}

echo $generator->generate_text();

