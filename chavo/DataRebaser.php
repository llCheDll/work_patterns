<?php
class DataRebaser{
    public $path_seo_files = 'seo_files/';
    public $path_generated_files = 'files/';
    
    public $html;
    public $keys;
    
    public function __construct() {
        $this->keys = array();
    }
    
    public function getSeoFileNames(){
        $file_names = scandir($this->path_seo_files);
        
        foreach ($file_names as $index => $file_name) {
            if ( $file_name[0] == '.' ) {
                unset($file_names[$index]);
            }
        }
        
        return $file_names;
    }
    
    public function getKeys($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $ul_flag = 0;
        
        foreach ($file as $key=>$str) {
            $tmp = str_getcsv($str,",");
            array_push($this->keys, $tmp);
        }
    }
    
    public function addLi($text, $href) {
        $html_pattern = '<li><a href="%s">%s</a></li>';
        $html = sprintf($html_pattern, $href, $text);
        $this->html .= $html;
    }
    
    public function csv_to_array($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        
        foreach ($file as $key=>$str) {
            $tmp = str_getcsv($str,",");
            array_push($keys, $tmp);
        }
        $keys = '<?php return ' . var_export($keys, true) . '?>';
        file_put_contents(
            $this->path_generated_files . explode('.', $filename)[0] . '.txt', $keys
        );
    }
    
    public function getHtml($filename){
        $ul_flag = 0;
        $tmp = array();
        foreach ($this->keys as $key => $value) {
            if ($value[0]) {
                array_push($tmp, array($value[0], $value[1]));
            } elseif ($value[1]) {
                array_push($tmp, array($value[1], $value[2]));
            } elseif ($value[2]) {
                array_push($tmp, array($value[2], $value[3]));
            } elseif ($value[3]) {
                array_push($tmp, array($value[3], $value[4]));
            }
        }
        
        $keys = '<?php return ' . var_export($tmp, true) . '?>';
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.txt', $keys
        );
        // return $tmp;
        
    }
}
