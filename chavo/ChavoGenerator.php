<?php 

class ChavoGenerator {
    public $path_generated_files;
    public $curr_uri;
    public $curr_host;
    public $full_url;
    public $current_h1;
    
    public $lang_version;
    
    public $show_aliases;
    public $masseurs;
    public $services;
    
    public $chavo_items;
    public $chavo_pattern;

    public function __construct($lang_version, $host, $curr_uri, $current_h1) {
        $this->lang_version = $lang_version;
        $this->path_generated_files = 'files/' . $this->lang_version . '/';
        $this->curr_uri = $curr_uri;
        $this->curr_host = $host;
        $this->current_h1 = $current_h1;
        
        $this->show_aliases = $this->listInclude(
            $this->path_generated_files . 'show_aliases_' . $this->lang_version . '.txt'
        );
        $this->chavo_pattern = $this->listInclude(
            $this->path_generated_files . 'chavo_' . $this->lang_version . '.txt'
        );
    }
    
    public function listInclude($filepath) {
        return include($filepath);
    }
    
    public function getChavoItems(){
        $file = file_get_contents(
            $this->path_generated_files . 'chavo_items_' . $this->lang_version . '.txt'
        );
        
        
        $this->chavo_items = unserialize($file);
        
        
        if (!$this->chavo_items) {
            $this->chavo_items = array();
        }
        
        return true;
    }
    
    public function setChavoItems($crypt_uri, $items){
        $this->chavo_items[$crypt_uri] = $items;
        $tmp = serialize($this->chavo_items);
        
        file_put_contents(
            $this->path_generated_files . 'chavo_items_' . $this->lang_version . '.txt', $tmp
        );
    }
    
    public function getMasseurs($counter=3){
        $tmp_array = array();
        
        $uri = '/masseurs/';
        
        if ($this->lang_version == 'ua') {
            $uri = '/ua/masazhysty/';
        }
        
        $keys = array_rand($this->masseurs, $counter);
        
        foreach ($keys as $key) {
            $tmp_item = $this->masseurs[$key];
            $tmp_uri = $uri . $tmp_item[0];
            
            array_push($tmp_array, array($tmp_uri, $tmp_item[1]));
        }
        
        return $tmp_array;
    }
    
    public function getServices($counter=4){
        $tmp_array = array();
        
        $uri = '/';
        
        if ($this->lang_version == 'ua') {
            $uri = '/ua/';
        }
        
        $keys = array_rand($this->services, $counter);
        
        foreach ($keys as $key) {
            $tmp_item = $this->services[$key];
            $tmp_uri = $uri . $tmp_item[1];
            
            array_push($tmp_array, array($tmp_uri, $tmp_item[0]));
        }
        
        return $tmp_array;
    }
    
    public function setListst(){
        $this->masseurs = $this->listInclude(
            $this->path_generated_files . 'masseurs_' . $this->lang_version . '.txt'
        );
        
        $this->services = $this->listInclude(
            $this->path_generated_files . 'services_' . $this->lang_version . '.txt'
        );
    }
    
    public function isShown(){
        
        $full_url = 'https://' . $this->curr_host . $this->curr_uri;
        
        foreach ($this->show_aliases as $alias) {
            if ($alias[1] == $full_url) {
                return true;
            }
        }
        return false;
    }
    
    public function getExistData(){
        $crypt_uri = base64_encode($this->curr_uri);
        
        if (array_key_exists($crypt_uri, $this->chavo_items)){
            return $this->chavo_items[$crypt_uri];
        }
        
        return false;
    }
    
    public function getData(){
        if ($this->isShown()){
            $this->getChavoItems();

            $data = $this->getExistData();
            
            if ($data){
                return $data;
            } else {
                $this->setListst();
                
                $crypt_uri = base64_encode($this->curr_uri);
                
                $tmp_array = array(
                    'masseurs'=>$this->getMasseurs(), 'services'=>$this->getServices()
                );
                $this->setChavoItems($crypt_uri, $tmp_array);
                
                return $tmp_array;
            }
        } else {
            return false;
        }
    }
    
    public function getHTML(){
        $data = $this->getData();
        
        $prepared_data = array();
        
        foreach ($data as $key => $value) {
            if($key == 'services') {
                array_push($prepared_data, $this->current_h1);
                array_push($prepared_data, $this->current_h1);
            }
            foreach ($value as $sub_value) {
                foreach ($sub_value as $tmp) {
                    array_push($prepared_data, $tmp);
                }
            }
        }
        
        var_dump(vsprintf($this->chavo_pattern, $prepared_data));die;
    }
}
