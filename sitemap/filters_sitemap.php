<?php
//файл с урлами
$track_visits_filters = file_get_contents('filename');


$URLS_PER_FILE = 45000;
$urls_count = count($track_visits_filters);
$sitemap_filters_files_count = round($urls_count/$URLS_PER_FILE);

$indexes_contents = '<?xml version="1.0" encoding="UTF-8"?>
    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
$url_number = 0;
for ($i = 1; $i <= $sitemap_filters_files_count ; $i++) { 
    $indexes_contents .= '
        <sitemap>
            <loc>'. $modx->config['site_url'].'sitemap-filters-'.$i.'.xml</loc>
        </sitemap>
    ';
    $sitemap_content = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';
    $url_number_trigger = 1;    
    while ($url_number_trigger < $URLS_PER_FILE){
        $sitemap_content .= '
            <url>
                <loc>'. $track_visits_filters[$url_number] .'</loc>
            </url>
        ';
        $url_number_trigger++;
        $url_number++;
        if ($url_number == $urls_count){
            break;
        }
    }
    $sitemap_content .= '</urlset>';
    file_put_contents(MODX_BASE_PATH.'sitemap-filters-'.$i.'.xml', $sitemap_content);
}

$indexes_contents .= '
    </sitemapindex>
';
file_put_contents(MODX_BASE_PATH.'sitemap-filters.xml', $indexes_contents);

echo $indexes_contents;
