<?php 
//composer require phpoffice/phpspreadsheet
require 'vendor/autoload.php';


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
$spreadsheet = $reader->load("/Users/macbook/work/work_patterns/sitemap/fitnessmir.xlsx");
$worksheet = $spreadsheet->getActiveSheet()->toArray();
$i = 0;
$n = 0;
$k = 0;
$e = 0;
$a = 0;
$b = 0;
$c = 0;
$s = 0;
foreach ($worksheet as $row) {
	// if($i > 1){
	// 	break;
	// }
	if($row[0] != null){
		$i++;
		$n = 0;
		$data_tmp[$i]['name'] = $row[0];
		$data_tmp[$i]['link'] = $row[1];
	}
	if($row[0] == null && $row[1] != null){
		$n++;
		$k = 0;
		$data_tmp[$i]['child'][$n]['name'] = $row[1];
		$data_tmp[$i]['child'][$n]['link'] = $row[2];
	}
	if($row[0] == null && $row[1] == null && $row[2] != null ){
		$k++;
		$e = 0;
		$data_tmp[$i]['child'][$n]['child'][$k]['name'] = $row[2];
		$data_tmp[$i]['child'][$n]['child'][$k]['link'] = $row[3];
	}
	if($row[0] == null && $row[1] == null && $row[2] == null && $row[3] != null ){
		$e++;
		$a = 0;
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['name'] = $row[3];
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['link'] = $row[4];
	}
	if($row[0] == null && $row[1] == null && $row[2] == null && $row[3] == null && $row[4] != null ){
		$a++;
		$b = 0;
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['child'][$a]['name'] = $row[4];
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['child'][$a]['link'] = $row[5];
	}
	if($row[0] == null && $row[1] == null && $row[2] == null && $row[3] == null && $row[4] == null && $row[5] != null ){
		$b++;
		$s = 0;
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['child'][$a]['child'][$b]['name'] = $row[5];
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['child'][$a]['child'][$b]['link'] = $row[6];
	}
	if($row[0] == null && $row[1] == null && $row[2] == null && $row[3] == null && $row[4] == null && $row[5] == null && $row[6] != null){
		$s++;
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['child'][$a]['child'][$b]['child'][$s]['name'] = $row[6];
		$data_tmp[$i]['child'][$n]['child'][$k]['child'][$e]['child'][$a]['child'][$b]['child'][$s]['link'] = $row[7];
	}
}
$string = '';
foreach ($data_tmp as $key => $value) {
	$string .= '<ul><a href="'.$value['link'].'">'.$value['name'].'</a>';
	foreach ($value['child'] as $key_1 => $value_1) {
		$string .= '<li><a href="'.$value_1['link'].'">'.$value_1['name'].'</a>';
		$string .= '<ul>';
			foreach ($value_1['child'] as $key_2 => $value_2) {
				$string .= '<li><a href="'.$value_2['link'].'">'.$value_2['name'].'</a>';
				$string .= '<ul>';
				foreach ($value_2['child'] as $key_3 => $value_3) {
					$string .= '<li><a href="'.$value_3['link'].'">'.$value_3['name'].'</a>';
					$string .= '<ul>';
					foreach ($value_3['child'] as $key_4 => $value_4) {
						$string .= '<li><a href="'.$value_4['link'].'">'.$value_4['name'].'</a>';
						$string .= '<ul>';
						foreach ($value_4['child'] as $key_5 => $value_5) {
							$string .= '<li><a href="'.$value_5['link'].'">'.$value_5['name'].'</a>';
							$string .= '<ul>';
							foreach ($value_5['child'] as $key_6 => $value_6) {
								$string .= '<li><a href="'.$value_6['link'].'">'.$value_6['name'].'</a>';
							}
							$string .= '</ul>';
							$string .= '</li>';
						}
						$string .= '</ul>';
						$string .= '</li>';
					}
					$string .= '</ul>';
					$string .= '</li>';
				}
				$string .= '</ul>';
				$string .= '</li>';
			}
		$string .= '</ul>';
		$string .= '</li>';
	}
	$string .= '</ul>';

}
file_put_contents('sitemap.html', $string);
die;
