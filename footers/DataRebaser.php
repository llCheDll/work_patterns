<?php
class DataRebaser{
    public $file_names;
    
    public $path_seo_files = 'seo_files/';
    public $path_genereted_files = 'files/';
    
    // public $path_seo_files = THINK_CLIENT_ROOT_PATH.'/data/footers_seo_files/';
    // public $path_genereted_files = THINK_CLIENT_ROOT_PATH.'/data/footers_static_data/';
    
    function __construct(){
        $this->file_names = $this->getSeoFileNames();
    }
    
    public function getSeoFileNames(){
        $file_names = scandir($this->path_seo_files);
        
        foreach ($file_names as $index => $file_name) {
            if ( $file_name[0] == '.' ) {
                unset($file_names[$index]);
            }
        }
        
        return $file_names;
    }
    
    // Functionn rewrite file
    // array(text=>href)
    // 1 column [text] 2 column [href]
    public function top_any_to_kvarray($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        
        foreach ($file as $str) {
            $tmp = str_getcsv($str,",");
            $keys[$tmp[0]] = $tmp[1];
        }
        
        $keys = serialize($keys);
        
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.php', $keys
        );
    }
    
    public function top_menu_to_kvarray($filename){
        $file = file($this->path_seo_files . '/' . $filename);
        $keys = array();
        
        $menu = '';
        $menu_href = '';
        $sub_menu = '';
        $href = '';
        
        foreach ($file as $str) {
            $tmp = str_getcsv($str,",");
            
            if ($tmp[0]) {
                $menu = $tmp[0];
                $menu_href = $tmp[1];
                $keys[$menu] = array('url'=>$menu_href, 'childs'=>array());
            } else if ($tmp[1]) {
                $sub_menu = $tmp[1];
                $href = $tmp[2];
                $keys[$menu]['childs'][$sub_menu] = $href;
            }
        }
        
        $keys = serialize($keys);
        file_put_contents(
            $this->path_genereted_files . explode('.', $filename)[0] . '.php', $keys
        );
    }
    
    public function rebase_all(){
        foreach($this->file_names as $filename){
            if ($filename == 'top_menu.csv') {
                $this->top_menu_to_kvarray($filename);
            } else {
                $this->top_any_to_kvarray($filename);
            }
        }
    }
}

$rebaser = new DataRebaser();

$rebaser->rebase_all();

// $file = file_get_contents('files/top_cards.php');
// var_dump(unserialize($file));die;

