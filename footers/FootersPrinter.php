<?php

class FootersPrinter {
    private $footers_cache_data;
    
    private $footers_items = array(
        'top_menu' => 'ТОП меню',
        'top_cards' => 'ТОП карточки товаров',
        'top_categories' => 'ТОП категории',
        'top_filters' => 'ТОП фильтры',
    );

    private $footers_settings = array(
        'top_cards' => array(
        		'items_total' => 24,
        		'items_per_column' => 8,
        	),
        'top_categories' => array(
        		'items_total' => 32,
        		'items_per_column' => 8,
        	),
        'top_filters' => array(
        		'items_total' => 24,
        		'items_per_column' => 8,
        	),
    );
    
    private $curr_uri;
    
    function __construct(){
        $this->curr_uri = $_SERVER['REQUEST_URI'];
    }

    public function get_footers_cache_data(){
        foreach ($this->footers_items as $key => $value) {
            $file_name = THINK_CLIENT_ROOT_PATH.'/data/footers_cache/'.$key.'.php';
            if(file_exists($file_name)){
                $data = unserialize(file_get_contents($file_name));
                if(isset($data[$this->curr_uri]))
                    $this->footers_cache_data[$key] = $data[$this->curr_uri];
            }
            if(empty($this->footers_cache_data[$key])){
                $method = 'generate_data_'.$key;
                if(method_exists($this, $method))
                    $this->footers_cache_data[$key] = $this->$method();
                else
                    $this->footers_cache_data[$key] = $this->generate_data_default($key);

            }
        }
    }

    public function generate_data_default($block_name){
        $data = unserialize(file_get_contents(THINK_CLIENT_ROOT_PATH.'/data/footers_static_data/'.$block_name.'.php'));
        $block_data = array();
        $items_total = $this->footers_settings[$block_name]['items_total'];
        $i = 1;
        while (count($block_data) < $items_total) {
            if($i>50)
                break;
            $k = array_rand($data);
            $v = $data[$k];
            $block_data[$k] = $v;
            unset($data[$k]);
            $i++;
        }
        if(count($block_data) == $items_total){
            $file_name = THINK_CLIENT_ROOT_PATH.'/data/footers_cache/'.$block_name.'.php';
            if(!file_exists($file_name))
                file_put_contents($file_name, serialize(array()));

            $cached_data = unserialize(file_get_contents($file_name));
            $cached_data[$this->curr_uri] = $block_data;
            file_put_contents($file_name, serialize($cached_data));
        }
        return $block_data;
    }

    public function generate_data_top_menu(){
        $data = unserialize(file_get_contents(THINK_CLIENT_ROOT_PATH.'/data/footers_static_data/top_menu.php'));

        return $data;
    }

    public function print_footers(){
        // if(strpos($this->html, '<!--footers_block-->') !== false){
        $footers_html = $this->get_footers_html();
        return $footers_html;
        //     $this->html = str_replace('<!--footers_block-->', $footers_html, $this->html);
        // }
    }

    public function get_footers_html(){
        $this->get_footers_cache_data();
        $footers_header_items_html = $this->get_footers_header_items_html();
        $footers_body_items_html = $this->get_footers_body_items_html();
        $footers_html = '';
        $footers_html .= $this->get_footers_assets();
        $footers_html .= '
            <div class="losb-block">
                <div class="losb-header">
                    <span class="losb-title"></span>
                    <ul class="losb-menu">
                        '.$footers_header_items_html.'
                    </ul>
                </div>
                <div class="losb-body">
                '.$footers_body_items_html.'
                </div>
            </div>
        ';
        return $footers_html;
    }

    public function get_footers_header_items_html(){
        $i = 1;
        $footers_header_items_html = '';
        foreach ($this->footers_items as $key => $value) {
            if(empty($this->footers_cache_data[$key]))
                continue;
            $footers_header_items_html .= '<span class="losb-menu-element '.(($i == 1)?'active':'').'" data-id="'.$key.'">'.$value.'</span>';
            $i++;
        }
        return $footers_header_items_html;
    }

    public function get_footers_body_items_html(){
        $i = 1;
        $footers_body_items_html = '';
        foreach ($this->footers_items as $key => $value) {
        	if(empty($this->footers_cache_data[$key]))
                continue;
            if(method_exists($this, 'print_'.$key)){
                $method = 'print_'.$key;
                $footers_body_items_html .= $this->$method();
            }else{
                $footers_body_items_html .= $this->print_default($key);
            }
            $i++;
        }
        return $footers_body_items_html;
    }

    public function get_footers_assets(){
        return "<style type=\"text/css\">.losb-block{padding-left: 2.5rem; margin-bottom: 2.5rem;} .losb-link { display: block;font-size: 14px;font-style: normal;padding: 1px 0;line-height: 17px;text-decoration: none;color: rgb(255, 255, 255);transition: 0.3s;font-family: inherit;} .losb-link:hover { color: rgb(198, 18, 18); } .losb-menu-result { display: inline-grid; width: auto; padding-right: 20px; white-space: normal; } .losb-content-sub { height: 0px; overflow: hidden; } .losb-content-sub.active {height: auto; overflow: auto; } .losb-menu-element-sub {  display: block;font-size: 14px;font-style: normal;padding: 4px 0;line-height: 16px;cursor: pointer;color: rgb(255, 255, 255);font-weight: 600;transition: 0.3s; } .losb-content-sub { font-size: 13px; font-style: normal; line-height: 16px; cursor: pointer; color: gray; transition: 0.3s; } .losb-menu-element-sub:hover, .losb-content-sub { color: rgb(198, 18, 18); } .losb-menu-element-sub.active { color: rgb(198, 18, 18); border-right: 2px solid rgb(198, 18, 18); } .losb-content-level-basic {display: flex; padding-left: 3px; } .losb-content-level-left { display: inline-grid; width: 175px; padding-left: 3px; vertical-align: top; border-right: 1px solid lightgray; } .losb-content-level-right { padding-left: 10px; } .losb-content-level-left, .losb-content-level-right { display: inline-grid; } .losb-menu { margin: 0 0 24px; padding: 0; padding-top: 10px; list-style-type: none; } .losb-menu-element { border-radius: 3px;border-color: #fff; z-index: 1; position: relative; height: 24px; border: 1px solid #888; color: #fff; opacity:0.8;background: transparent; display: inline-grid; margin: 0; margin-right:3px; padding: 0 15px; line-height: 24px; font-size: 13px; margin-left: 0px; text-decoration: none; cursor: pointer; vertical-align: middle; } .losb-menu-element.active {opacity: 1; color: #fff;border-radius: 3.2px;border-color: #fff;font-weight: 600;} .losb-content-sub { font-size: 14px; } .losb-title { margin: 24px 0 12px; padding: 0; line-height: 28px; font-weight: 500; font-size: 24px; } .losb-content { height: 0px; overflow: hidden; } .losb-content.active { height: auto; overflow: auto;  } .losb-content-level-basic h2{font-weight: bold;font-size: 20px;}.losb-content-level-basic ul li{list-style: disc;margin-left: 15px;}</style> <script type=\"text/javascript\"> jQuery(document).ready(function() { jQuery('.losb-menu-element').on('click', function() { jQuery('.losb-menu-element').removeClass('active'); jQuery(this).addClass('active'); var id = jQuery(this).attr('data-id'); if(id) { switchLosb(id); } }); jQuery('.losb-menu-element-sub').on('click', function(){ jQuery('.losb-menu-element-sub').removeClass('active'); jQuery(this).addClass('active'); var id = jQuery(this).attr('data-id'); if(id) { switchLosbSub(id); } }); function switchLosb(id) { jQuery('.losb-content').removeClass('active'); jQuery('.losb-content[data-id=\"losb-content-'+id+'\"]').addClass('active'); } function switchLosbSub(id) { jQuery('.losb-content-sub').removeClass('active'); jQuery('.losb-content-sub[data-id=\"losb-content-sub-'+id+'\"]').addClass('active'); } }); </script>";
    }

    public function print_default($block_name){
        $html = '';
        $items_per_column = $this->footers_settings[$block_name]['items_per_column'];
        $items_total = $this->footers_settings[$block_name]['items_total'];

    	$column_count = ceil($items_total/$items_per_column);
    	$column_width_percangate = round(100/$column_count);
    	$style = 'style="width:'.$column_width_percangate.'%;"';

        $i = 1;
        $items = array();
        $columns = array();
        foreach ($this->footers_cache_data[$block_name] as $item_name => $item_url) {
            if($i > $items_per_column ){
                $i = 1;
                $columns[] = '<div class="losb-menu-result" '.$style.'>'.implode('', $items).'</div>';
                $items = array();
            }
            $items[] = '<a class="losb-link" href="'.$item_url.'">'.$this->mb_ucfirst($item_name).'</a>';
            $i++;
        }
        $columns[] = '<div class="losb-menu-result" '.$style.'>'.implode('', $items).'</div>';
        
        $html = '<div class="losb-content" data-id="losb-content-'.$block_name.'">';
        $html .=     '<div class="losb-content-level-basic">'.implode('', $columns).'</div>';
        $html .= '</div>';

        return $html;
    }

    public function print_top_menu(){
        $top_menu_html = '';
        if(!empty($this->footers_cache_data['top_menu'])){
            $left_column = '';
            $right_column = '';
            $i = 1;
            $left_column_items = array();
            $right_column_items = array();
            foreach ($this->footers_cache_data['top_menu'] as $title => $value) {
                $left_column_items[] = 
                    '<div class="losb-menu-element-sub '.(($i == 1)?'active':'').'" data-id="'.$i.'">'.
                    (
                        ($value['url'])? '<a href="'.$value['url'].'">'.$title.'</a></div>': 
                        $title.'</div>'
                    );
                $items_count = count($value['childs']);
                $big_half = ceil($items_count/3);
                $ii = 1;
                $items_block_html = array();
                $items_html = array();
                foreach ($value['childs'] as $item_name => $item_url) {
                    if($ii > $big_half ){
                        $ii = 1;
                        $items_block_html[] = '<div class="losb-menu-result">'.implode('', $items_html).'</div>';
                        $items_html = array();
                    }
                    $items_html[] = '<a class="losb-link" href="'.$item_url.'">'.$item_name.'</a>';
                    $ii++;
                }
                $items_block_html[] = '<div class="losb-menu-result">'.implode('', $items_html).'</div>';
                $right_column_items[] = '<div class="losb-content-sub '.(($i == 1)?'active':'').'" data-id="losb-content-sub-'.$i.'">'.implode('', $items_block_html).'</div>';
                $i++;
            }
            $top_menu_html = '<div class="losb-content active" data-id="losb-content-top_menu">';
            $top_menu_html .=    '<div class="losb-content-level-left">';
            $top_menu_html .=       implode('', $left_column_items);
            $top_menu_html .=    '</div>';
            $top_menu_html .=    '<div class="losb-content-level-right">';
            $top_menu_html .=       implode('', $right_column_items);
            $top_menu_html .=    '</div>';
            $top_menu_html .= '</div>';
        }
        return $top_menu_html;
    }

    public function mb_ucfirst($str, $encoding='UTF-8')
    {
        $str = mb_ereg_replace('^[\ ]+', '', $str);
        $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).
               mb_substr($str, 1, mb_strlen($str), $encoding);
        return $str;
    }
}
